
let autocomplete;
let streetField;
let apptField;
let zipCodeField;

function initAutocomplete() {
  streetField = document.querySelector("#street-address");
  apptField = document.querySelector("#address2");
  zipCodeField = document.querySelector("#zipCode");
  if(navigator.geolocation){
    navigator.geolocation.getCurrentPosition(function (position) {

      var geolocation = new google.maps.LatLng(
      position.coords.latitude, position.coords.longitude);
      var circle = new google.maps.Circle({
          center: geolocation,
          radius: position.coords.accuracy
      });
      autocomplete.setBounds(circle.getBounds());

      // Log autocomplete bounds here
      console.log(autocomplete.getBounds());
  });
  }

  autocomplete = new google.maps.places.Autocomplete(streetField, {
    componentRestrictions: { country: ["ca"] },
    fields: ["address_components"],
    types: ["address"],
    
  });
  streetField.focus();
  autocomplete.addListener("place_changed", getAddress);
}

function getAddress() {
 
  const place = autocomplete.getPlace();
  let streetAddress = "";
  let zipCode = "";

  for (const component of place.address_components) {
    
    const componentType = component.types[0];

    switch (componentType) {
      case "street_number": {
        streetAddress = `${component.long_name} ${streetAddress}`;
        break;
      }
      case "route": {
        streetAddress += component.short_name;
        break;
      }
      case "postal_code": {
        zipCode = `${component.long_name}${zipCode}`;
        break;
      }
      case "postal_code_suffix": {
        zipCode = `${zipCode}-${component.long_name}`;
        break;
      }
      case "locality":{
        document.querySelector("#city").value = component.long_name;
        break;
      }
      case "administrative_area_level_1": {
        document.querySelector("#state").value = component.short_name;
        break;
      }
      case "country":
        document.querySelector("#country").value = component.long_name;
        break;
    }
  }
  streetField.value = streetAddress;
  zipCodeField.value = zipCode;
  apptField.focus();
  console.log(place.address_components);
}

window.initAutocomplete = initAutocomplete;
